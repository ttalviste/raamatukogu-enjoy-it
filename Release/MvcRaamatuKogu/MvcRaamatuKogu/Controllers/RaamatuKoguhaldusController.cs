﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;

namespace MvcRaamatuKogu.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RaamatuKoguhaldusController : Controller
    {
        private RaamatuKoguEntities db = new RaamatuKoguEntities();

        //
        // GET: /RaamatuKoguhaldus/

        public ViewResult Index(string sortOrder)
        {
            ViewBag.TitleSortParm = String.IsNullOrEmpty(sortOrder) ? "Title desc" : "";
            ViewBag.AutorSortParm = sortOrder == "Autor" ? "Autor desc" : "Autor";
            var items = from x in db.Items
                        select x;
            switch (sortOrder)
            {
                case "Title ˇ":
                    items = items.OrderByDescending(s => s.Title);
                    break;
                case "Autor":
                    items = items.OrderBy(s => s.Autors.Perekonnanimi);
                    break;
                case "Autor ˇ":
                    items = items.OrderByDescending(s => s.Autors.Perekonnanimi);
                    break;
                default:
                    items = items.OrderBy(s => s.Title);
                    break;
            }
            return View(items.ToList());
        }

        //
        // GET: /RaamatuKoguhaldus/Details/5

        public ViewResult Details(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // GET: /RaamatuKoguhaldus/Edit/5

        public ActionResult Edit(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // POST: /RaamatuKoguhaldus/Edit/5

        [HttpPost]
        public ActionResult Edit(Item item)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(item);
        }

        //
        // GET: /RaamatuKoguhaldus/Delete/5

        public ActionResult Delete(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // POST: /RaamatuKoguhaldus/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}