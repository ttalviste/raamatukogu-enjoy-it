﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;
using System.Collections.ObjectModel;
using System.Data;
using System.Web.Security;
using System.IO;
using System.Web.Helpers;

namespace MvcRaamatuKogu.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        RaamatuKoguEntities db = new RaamatuKoguEntities();

        //
        // GET: /Profile/
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                MembershipUser kasutaja = Membership.GetUser();
                Guid userID = (Guid)kasutaja.ProviderUserKey;
                string user = User.Identity.Name;
                var itemid = db.UserItems.Where(i => i.UserId == userID).ToList();
                return View(itemid);
            }
            return View();
        }

        #region KastuajaRaamatuHaldus
        //
        // GET: /Profile/Details/5
        public ActionResult Book(int id = 0)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);


        }
        //
        // GET: /Profile/LisaRaamat/
        public ActionResult LisaRaamat()
        {

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name");
            return View();
        }

        //
        // POST: /Profile/LisaRaamat/
        [HttpPost]
        public ActionResult LisaRaamat(Item item)
        {
            MembershipUser kasutaja = Membership.GetUser();
            Guid userID = (Guid)kasutaja.ProviderUserKey;
            try
            {
                if (ModelState.IsValid)
                {
                    Autor autor = new Autor()
                    {
                        Perekonnanimi = item.Autors.Perekonnanimi
                    };
                    db.Autors.Add(autor);
                    db.SaveChanges();
                    var autorId = (from x in db.Autors
                                   where x.Perekonnanimi == item.Autors.Perekonnanimi
                                   select x.AutorId).FirstOrDefault();

                    item.Lisatud = DateTime.Now;
                    item.CoverArtUrl = "/UploadedImages/placeholder.gif";
                    item.AutorId = autorId;
                    db.Items.Add(item);
                    db.SaveChanges();


                    var itm = (from x in db.Items
                               where x.ItemId == item.ItemId
                               select x.ItemId).FirstOrDefault();
                    string _user = User.Identity.Name;

                    var kasutajaID = (from z in db.Kasutajad
                                      where z.fk_UserId == userID
                                      select z.KasutajaId).First();
                    UserItem userItem = new UserItem()
                    {
                        Adder = _user,
                        ItemId = itm,
                        UserId = userID,
                        Added = DateTime.Now,
                        fk_KasutajaId = kasutajaID


                    };
                    db.UserItems.Add(userItem);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException e)
            {

                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", e.ToString());
            }


            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name", item.CategoryId);
            return View(item);
        }

        //
        // GET: /Profile/Muuda/5
        public ActionResult Muuda(int id)
        {

            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId =
                new SelectList(db.Categories, "CategoryId", "Name");
            ViewBag.AutorId =
                new SelectList(db.Autors, "AutorId", "Perekonnanimi");
            return View(item);

        }

        //
        // POST: /Profile/Muuda/5
        [HttpPost]
        public ActionResult Muuda(int id, Item item)
        {
            try
            {
                item.ItemId = id;
                var image = WebImage.GetImageFromRequest();
                if (image != null)
                {
                    if (image.Width > 500)
                    {
                        image.Resize(100, ((100 * image.Height) / image.Width));

                    }
                    var filename = Path.GetFileName(image.FileName);
                    image.Save(Server.MapPath("~/UploadedImages/" + filename));
                    filename = Path.Combine("UploadedImages", filename);
                    item.CoverArtUrl = filename;
                }
                if (ModelState.IsValid)
                {


                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {

                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.CategoryId =
               new SelectList(db.Categories, "CategoryId", "Name", item.CategoryId);
            ViewBag.AutorId =
                new SelectList(db.Autors, "AutorId", "Perekonnanimi", item.AutorId);
            return View(item);
        }
        #endregion

        #region KasutajaAndmeteHaldus
        //
        // GET: /LisaAndmed/5
        public ActionResult LisaAndmed()
        {
            return View();
        }

        //
        // POST: /LisaAndmed/5
        [HttpPost]
        public ActionResult LisaAndmed(KasutajaAndmed kasutajaAndmed)
        {
            MembershipUser kasutaja = Membership.GetUser();
            Guid userID = (Guid)kasutaja.ProviderUserKey;
            DateTime nyyd = DateTime.Now;
            if (ModelState.IsValid)
            {
                kasutajaAndmed.Added = nyyd;
                kasutajaAndmed.UserID = userID;
                db.KasutajaAndmed.Add(kasutajaAndmed);
                db.SaveChanges();

                var detailsId = (from x in db.KasutajaAndmed
                                 where x.UserID == userID
                                 select x.Id).FirstOrDefault();

                Kasutaja uusKasutaja = new Kasutaja()
                {
                    fk_KasutajaAndmedId = detailsId,
                    fk_UserId = userID,
                    Added = nyyd,
                    Modified = nyyd,
                    IsActive = true

                };
                db.Kasutajad.Add(uusKasutaja);
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            return View();
        }
        
        [ChildActionOnly]
        public ActionResult UserInfoMenu()
        {
            MembershipUser kasutaja = Membership.GetUser();

            return View(kasutaja);
        }
        //
        // GET: /Andmed/5
        public ActionResult Andmed(Guid userID)
        {
            var tmp = (from x in db.KasutajaAndmed
                       where x.UserID == userID
                       select x.Id).FirstOrDefault();
            var kasutaja = db.KasutajaAndmed.Find(tmp);
            return View(kasutaja);
        }
#endregion

        #region Kaanepiltide mudeli edasi arendus(Nice TO Have)
        // GET: /AddCover
        //public ActionResult addCover(int ItemId)
        //{

        //    return View();

        //}
        //
        // POST: Image
        //public ActionResult addCover(int ItemId, CoverArt cover)
        //{
        //    RaamatuKoguEntities db = new RaamatuKoguEntities();
        //    var raamat = (from x in db.Items
        //                  where x.ItemId == ItemId
        //                  select x).FirstOrDefault();
        //    var image = WebImage.GetImageFromRequest();
        //    if (image != null)
        //    {
        //        if (image.Width > 500)
        //        {
        //            image.Resize(500, ((500 * image.Height) / image.Width));
        //        }

        //        var filename = Path.GetFileName(image.FileName);
        //        image.Save(Path.Combine("../UploadedImages", filename));
        //        filename = Path.Combine("../UploadedImages", filename);

        //        cover.coverArtUrl = Url.Content(filename);

        //        CoverArt pilt = new CoverArt()
        //        {
        //            coverArtUrl = filename,
        //            coverBookTitle = raamat.Title
        //        };

        //        db.CoverArts.Add(pilt);
        //        db.SaveChanges();

        //    }

        //    return View(cover);

        //}
        //
        // GET: /Laenutus/Create
        #endregion

        #region KasutajaLaentuseHaldus
        //
        // GET: /Profile/Laenraamat?itemId=6
        public ActionResult LaenaRaamat(int itemId)
        {
            Item item = db.Items.Find(itemId);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(new Laenutus());
        }

        //
        // POST: /Laenutus/Create

        [HttpPost]
        public ActionResult LaenaRaamat(int itemId, Laenutus laen)
        {
            try
            {
                MembershipUser kasutaja = Membership.GetUser();
                Guid userID = (Guid)kasutaja.ProviderUserKey;
                Item laenutatav = GetItem(itemId);
                if (ModelState.IsValid)
                {
                    DateTime DueDate = laen.Start.AddDays(laenutatav.days);
                    laen.fk_ItemId = laenutatav.ItemId;
                    laen.Laenutaja = User.Identity.Name;
                    laen.Added = DateTime.Now;
                    laen.End = DueDate;
                    laen.Modified = DateTime.Now;

                    db.Laenutused.Add(laen);
                    db.SaveChanges();

                    var fk_laentusID = (from x in db.Laenutused
                                        where x.fk_ItemId == itemId
                                        && x.End == DueDate
                                        && x.Laenutaja == User.Identity.Name
                                        select x.LaenutusId).First();
                    var fk_kasutajaID = (from x in db.Kasutajad
                                         where x.fk_UserId == userID
                                         select x.KasutajaId).First();

                    KasutajaLaenutuses uusLaenutaja = new KasutajaLaenutuses()
                    {
                        Added = DateTime.Now,
                        fk_laenutus_Id = fk_laentusID,
                        fk_kasutaja_Id = fk_kasutajaID

                    };
                    db.KasutajaLaenututes.Add(uusLaenutaja);
                    laenutatav.laenutatud = true;
                    db.SaveChanges();

                    return RedirectToAction("Booking", new { laenutusId = fk_laentusID  });
                }
                



            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }

        //
        // GET: /Profile/Booking?laenutusId = 5
        public ActionResult Booking(int laenutusId)
        {
            Laenutus laenutus = db.Laenutused.Find(laenutusId);
            if (laenutus == null)
            {
                return HttpNotFound();
            }
            return View(laenutus);
        }


            #region Abimeetodid
        Item GetUserItem(int UserId)
        {
            RaamatuKoguEntities db = new RaamatuKoguEntities();
            var itemUseril = (from x in db.UserItems
                              where x.fk_KasutajaId == UserId
                              select x.ItemId).First();
            Item KasutajaItem = GetItem(itemUseril);

            return KasutajaItem;

        }

        Item GetItem(int id)
        {
            RaamatuKoguEntities db = new RaamatuKoguEntities();
            var _item = (from x in db.Items
                         where x.ItemId == id
                         select x).FirstOrDefault();
            return _item;
        }
            #endregion

        #endregion KasutajaLaenutuseHaldus

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
