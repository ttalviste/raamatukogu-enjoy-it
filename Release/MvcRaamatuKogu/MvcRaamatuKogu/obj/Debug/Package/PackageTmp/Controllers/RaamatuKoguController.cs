﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;

namespace MvcRaamatuKogu.Controllers
{
    public class RaamatuKoguController : Controller
    {
        
        RaamatuKoguEntities db = new RaamatuKoguEntities();
        //
        // GET: /RaamatuKogu/
        public ActionResult Index()
        {

            var categorys = db.Categorys.ToList();
            return View(categorys);

        }

        //
        // GET: /Raamatukogu/Browse
        public ActionResult Browse(string category)
        {

            var categoryModel = db.Categorys.Include("Items")
                                .Single(g => g.Name == category);

            return View(categoryModel);



        }
        //
        // GET: /Raamatukogu/BrowseAutor
        public ActionResult BrowseAutor(string autor)
        {
            using (RaamatuKoguEntities db = new RaamatuKoguEntities())
            {
                var autorItems = db.Autors.Include("Autors")
                                .Single(g => g.Perekonnanimi == autor);
                              
                return View(autorItems);
            }
           

            



        }

        //
        // GET: /Store/Categorymenu
        [ChildActionOnly]
        public ActionResult CategoryMenu()
        {
            var categorys = db.Categorys.ToList();
            return PartialView(categorys);
        }

        //
        // GET: /Raamatukogu/Details
        public ActionResult Details(int id)
        {
            var item = db.Items.Find(id);

            return View(item);

        }


    }
}
