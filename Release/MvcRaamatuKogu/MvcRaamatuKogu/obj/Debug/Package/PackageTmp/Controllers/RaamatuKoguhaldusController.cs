﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;

namespace MvcRaamatuKogu.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class RaamatuKoguhaldusController : Controller
    {
        private RaamatuKoguEntities db = new RaamatuKoguEntities();

        //
        // GET: /RaamatuKoguhaldus/

        public ViewResult Index()
        {
            var items = db.Items.Include(i => i.Categories);
            return View(items.ToList());
        }

        //
        // GET: /RaamatuKoguhaldus/Details/5

        public ViewResult Details(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // GET: /RaamatuKoguhaldus/Edit/5

        public ActionResult Edit(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // POST: /RaamatuKoguhaldus/Edit/5

        [HttpPost]
        public ActionResult Edit(Item item)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(item);
        }

        //
        // GET: /RaamatuKoguhaldus/Delete/5

        public ActionResult Delete(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }

        //
        // POST: /RaamatuKoguhaldus/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}