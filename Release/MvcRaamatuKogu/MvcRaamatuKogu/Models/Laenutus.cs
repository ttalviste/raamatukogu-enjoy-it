﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.ComponentModel.DataAnnotations;

namespace MvcRaamatuKogu.Models
{
    public class Laenutus
    {
        [Key]
        public int LaenutusId { get; set; }

        public DateTime Added { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime Modified { get; set; }
        public int fk_ItemId { get; set; }
        public String Laenutaja { get; set; }

        [ForeignKey("fk_ItemId")]
        public virtual Item Laenutis { get; set; }


    }

    public class KasutajaLaenutuses
    {
        [Key]
        public int kasutaja_laenutuse_Id { get; set; }
        
        public DateTime Added { get; set; }

        public int fk_laenutus_Id { get; set; }
        public int fk_kasutaja_Id { get; set; }

        [ForeignKey("fk_laenutus_Id")]
        public virtual Laenutus KastuajaLaen { get; set; }
        [ForeignKey("fk_kasutaja_Id")]
        public virtual Kasutaja Laenaja { get; set; }
    }
}