﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;

namespace MvcRaamatuKogu.Models
{
    public class KasutajaAndmed
    {
        [Key]
        public int Id { get; set; }
        public Guid UserID { get; set; }

        [Required(ErrorMessage = "Eesnimi on sisestamata")]
        [Display(Name = "Eesnimi")]
        public string eesNimi { get; set; }

        [Required(ErrorMessage = "Perekonnanimi on sisestamata")]
        [Display(Name = "Perekonnanimi")]
        public string pereNimi { get; set; }

        [Required(ErrorMessage = "Sünnikuupäev on sisestamata")]
        [Display(Name = "Sünnikuupäev")]
        [DataType(DataType.Date)]
        public DateTime synniKuup { get; set; }

        public DateTime Added { get; set; }


    }
    public class Kasutaja
    {
        [Key]
        public int KasutajaId { get; set; }

        public Guid fk_UserId { get; set; }
        public int fk_KasutajaAndmedId { get; set; }

        public DateTime Added { get; set; }
        public DateTime Modified { get; set; }
        [Display(Name = "Kasutaja staatus")]
        public bool IsActive { get; set; }

        [ForeignKey("fk_KasutajaAndmedId")]
        public virtual KasutajaAndmed UserInfo { get; set; }
    }
}