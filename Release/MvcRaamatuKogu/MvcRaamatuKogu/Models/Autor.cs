﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcRaamatuKogu.Models
{
    public class Autor
    {
        [Key]
        public int AutorId { get; set; }
        [Display(Name = "Eesnimi")]
        public string Eesnimi { get; set; }
        [Display(Name = "Perekonnanimi")]
        [Required]
        public string Perekonnanimi { get; set; }
        public bool aktiivne { get; set; }

        
    }
}