﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcRaamatuKogu.Models
{
    #region Raamatu mudel
    [Bind(Exclude = "ItemId")]
    public class Item
    {
        [ScaffoldColumn(false)]
        [Key]
        public int ItemId{ get; set; }


        [Display(Name = "Kategooria")]
        public int CategoryId { get; set;}

        [Display(Name = "Autor")]
        public int AutorId { get; set; }

        [Required(ErrorMessage = "Raamatu nimi on kohustuslik !")]
        [StringLength(160)]
        public string Title { get; set; }

        public DateTime Lisatud { get; set; }

        public bool laenutatud { get; set; }

        [Display(Name="Kas on avalik?")]
        public bool IsPublic { get; set; }

        [Display(Name="Raamatu kaanepilt")]
        [DisplayName("Kaanepilt")]
        [StringLength(1024)]
        public string CoverArtUrl { get; set; }
        
        public bool aktiivne { get; set; }

        [DisplayName("Päevade arv, kui kauaks saab sisestada")]
        public int days { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Categories { get; set; }
        [ForeignKey("AutorId")]
        public virtual Autor Autors { get; set; }
       
        #region Kui peaks j6udma edasi arendada
        //[Display(Name = "Raamatu kaanepilt")]
        //public int CoverArtId { get; set; }
        //[ForeignKey("CoverArtId")]
        //public virtual CoverArt Covers { get; set; }
        #endregion


    }
    #endregion

    #region Kasutaja_Item
    public class UserItem
    {
        [Key]
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int fk_KasutajaId { get; set; }
        public Guid UserId { get; set; }
        public DateTime Added { get; set; }
        public string Adder { get; set; }

        [ForeignKey("ItemId")]
        public virtual Item Items { get; set; }
        [ForeignKey("fk_KasutajaId")]
        public virtual Kasutaja ItemsUser { get; set; }

    }
#endregion

#region Mudel Kaanepildile
    //public class CoverArt
    //{
    //    public int Id { get; set; }
    //    public string coverArtUrl { get; set; }
    //    public string coverBookTitle { get; set; }
    //    public DateTime created { get; set; }
    //}
#endregion
}