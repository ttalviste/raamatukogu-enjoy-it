﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Collections.ObjectModel;
using System.Data.Entity.ModelConfiguration;
using System.Web.Security;

namespace MvcRaamatuKogu.Models
{
    public class RaamatuKoguEntities : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Autor> Autors { get; set; }
        public DbSet<UserItem> UserItems { get; set; }
        public DbSet<KasutajaAndmed> KasutajaAndmed { get; set; }
        public DbSet<Laenutus> Laenutused { get; set; }
        public DbSet<KasutajaLaenutuses> KasutajaLaenututes { get; set; }
        public DbSet<Kasutaja> Kasutajad { get; set; }
     //   public DbSet<CoverArt> CoverArts { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                        .HasRequired(a => a.Categories)
                        .WithMany()
                        .HasForeignKey(u => u.CategoryId);

            modelBuilder.Entity<Item>()
                        .HasRequired(a => a.Autors)
                        .WithMany()
                        .HasForeignKey(u => u.AutorId);

            modelBuilder.Entity<UserItem>()
                        .HasRequired(a => a.Items)
                        .WithMany()
                        .HasForeignKey(u => u.ItemId).WillCascadeOnDelete();
                        
            

            //modelBuilder.Entity<Item>()
            //            .HasRequired(a => a.Covers)
            //            .WithMany()
            //            .HasForeignKey(u => u.CoverArtId);
            

        }
    }
    
}