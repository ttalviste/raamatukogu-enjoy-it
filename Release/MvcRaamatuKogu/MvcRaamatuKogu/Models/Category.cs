﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcRaamatuKogu.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }
        [Display(Name = "Kategooria nimetus")]
        public string Name { get; set; }
        [Display(Name = "Kirjeldus")]
        public string Description { get; set; }
        public bool aktiivne { get; set; }
        public List<Item> Items { get; set; }
        
    }
}