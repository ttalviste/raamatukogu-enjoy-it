﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

/// <summary>
/// Summary description for Sessioonid
/// </summary>
public class Sessioonid
{
    private Sessioonid()
    {

    }

    public static Sessioonid Current
    {
        get
        {
            Sessioonid sess = (Sessioonid)HttpContext.Current.Session["nyyd"];

            if (sess == null)
            {
                sess = new Sessioonid();
                HttpContext.Current.Session["nyyd"] = sess;
            }

            return sess;
        }
    }

    public Guid UserID { get; set; }
    
}