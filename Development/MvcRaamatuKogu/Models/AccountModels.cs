﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
//using System.Web.Security;

namespace MvcRaamatuKogu.Models
{

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Vana salasõna")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Uus salasõna")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita uus salasõna")]
        [Compare("NewPassword", ErrorMessage = "Sisestatud salasõnad ei ühti!")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Required]
        [Display(Name = "Kasutajanimi")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [Display(Name = "Mäleta mind?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Kasutajanimi")]
        [Remote("doesUserNameExist", "Account",
               HttpMethod = "POST",
               ErrorMessage = "Kasutajanimi on juba kasutusel!")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        //[Remote("doesEmailExist", "Account",
        //       HttpMethod = "POST",
        //       ErrorMessage = "Email on juba seotud teise kasutajaga")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} pikk.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Sisestatud salasõnad ei ühti!")]
        public string ConfirmPassword { get; set; }

        public int AutorId { get; set; }
        public virtual Autor Autor { get; set; }
    }
}
