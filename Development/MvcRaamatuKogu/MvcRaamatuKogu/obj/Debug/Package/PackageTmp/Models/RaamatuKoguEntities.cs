﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Collections.ObjectModel;

namespace MvcRaamatuKogu.Models
{
    public class RaamatuKoguEntities : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Autor> Autors { get; set; }
        public DbSet<UserItem> UserItems { get; set; }
        
        
        
    }
}