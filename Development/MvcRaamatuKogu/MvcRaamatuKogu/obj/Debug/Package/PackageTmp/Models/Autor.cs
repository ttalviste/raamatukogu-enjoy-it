﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcRaamatuKogu.Models
{
    public class Autor
    {
        public int AutorId { get; set; }
        [Display(Name = "Nimi")]
        public string Eesnimi { get; set; }
        public string Perekonnanimi { get; set; }

        
    }
}