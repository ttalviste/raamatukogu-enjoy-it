﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
namespace MvcRaamatuKogu.Models
{
    public class KasutajaAndmed
    {
        public int Id { get; set; }
        public Guid UserID { get; set; }
        
        [Required(ErrorMessage="Eesnimi on sisestamata")]
        [Display(Name="Eesnimi")]
        public string eesNimi { get; set; }
        
        [Required(ErrorMessage = "Perekonnanimi on sisestamata")]
        [Display(Name = "Perekonnanimi")]
        public string pereNimi { get; set; }

        [Required(ErrorMessage = "Sünnikuupäev on sisestamata")]
        [Display(Name = "Sünnikuupäev")]
        public DateTime synniKuup { get; set; }
        
        
    }
}