﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcRaamatuKogu.Models
{
    [Bind(Exclude = "ItemId")]
    public class Item
    {
        [ScaffoldColumn(false)]
        public int ItemId{ get; set; }


        [Display(Name = "Kategooria")]
        public int CategoryId { get; set; 
}
        public int AutorId { get; set; }

        [Required(ErrorMessage = "Raamatu nimi on kohustuslik !")]
        [StringLength(160)]
        public string Title { get; set; }

        public DateTime Lisatud { get; set; }

        public bool laenutatud { get; set; }

        [Display(Name="Kas on avalik?")]
        public bool IsPublic { get; set; }

        [DisplayName("Cover Art Url")]
        [StringLength(1024)] 
        public string CoverArtUrl { get; set; }
        public bool aktiivne { get; set; }
        
        [ForeignKey("CategoryId")]
        public virtual Category Categories { get; set; }
        [ForeignKey("AutorId")]
        public virtual Autor Autors { get; set; } 
        
       
    }
    public class UserItem
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public Guid UserId { get; set; }
       
        [ForeignKey("ItemId")]
        public virtual Item Items { get; set; }

    }
	
}