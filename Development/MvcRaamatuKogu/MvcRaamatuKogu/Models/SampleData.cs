﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MvcRaamatuKogu.Models;

namespace MvcRaamatuKogu.Models
{
    public class SampleData : DropCreateDatabaseAlways<RaamatuKoguEntities>
    {
        protected override void Seed(RaamatuKoguEntities context)
        {
            new List<Category>
            {
                new Category { Name = "Novel" },
                new Category { Name = "Fantasy" },
                new Category { Name = "Sci-Fi" },
                new Category { Name = "Misc" },
                new Category { Name = "Children" },
                new Category { Name = "Arts and Crafts" },
                new Category { Name = "Self help" },
                new Category { Name = "Popular" },
                new Category { Name = "Cooking" }
            }.ForEach(a => context.Categories.Add(a));
        }
    }
}