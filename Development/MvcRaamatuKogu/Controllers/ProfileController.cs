﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;
using System.Collections.ObjectModel;
using System.Data;
using System.Web.Security;
using System.IO;
using System.Web.Helpers;

namespace MvcRaamatuKogu.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        RaamatuKoguEntities db = new RaamatuKoguEntities();

        //
        // GET: /Profile/
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                MembershipUser kasutaja = Membership.GetUser();
                Guid userID = (Guid)kasutaja.ProviderUserKey;
                string user = User.Identity.Name;
                var itemid = db.UserItems.Where(i => i.UserId == userID).ToList();
                return View(itemid);
            }
            return View();
        }
        //
        // GET: /Profile/Details/5
        public ViewResult Book(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }
        //
        // GET: /Profile/LisaRaamat/
        public ActionResult LisaRaamat()
        {

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name");
            return View();
        }

        // POST: /Profile/LisaRaamat/
        [HttpPost]
        public ActionResult LisaRaamat(Item item)
        {
            MembershipUser kasutaja = Membership.GetUser();
            Guid userID = (Guid)kasutaja.ProviderUserKey;
            try
            {
                if (ModelState.IsValid)
                {
                    Autor autor = new Autor()
                    {
                        Perekonnanimi = item.Autors.Perekonnanimi
                    };
                    db.Autors.Add(autor);
                    db.SaveChanges();
                    var autorId = (from x in db.Autors
                                   where x.Perekonnanimi == item.Autors.Perekonnanimi
                                   select x.AutorId).FirstOrDefault();

                    item.Lisatud = DateTime.Now;
                    item.CoverArtUrl = "/UploadedImages/placeholder.gif";
                    item.AutorId = autorId;
                    db.Items.Add(item);
                    db.SaveChanges();


                    var itm = (from x in db.Items
                               where x.ItemId == item.ItemId
                               select x.ItemId).FirstOrDefault();
                    UserItem userItem = new UserItem()
                    {
                        ItemId = itm,
                        UserId = userID

                    };
                    db.UserItems.Add(userItem);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {

                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }


            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name", item.CategoryId);
            return View(item);
        }
        //
        // GET: /Profile/Muuda/5
        public ActionResult Muuda(int id)
        {

            Item item = db.Items.Find(id);
            ViewBag.CategoryId =
                new SelectList(db.Categories, "CategoryId", "Name");
            ViewBag.AutorId =
                new SelectList(db.Autors, "AutorId", "Perekonnanimi");
            return View(item);

        }

        //
        // POST: /Profile/Muuda/5
        [HttpPost]
        public ActionResult Muuda(int id, Item item)
        {
            try
            {
                item.ItemId = id;
                var image = WebImage.GetImageFromRequest();
                if (image != null)
                {
                    if (image.Width > 500)
                    {
                        image.Resize(100, ((100 * image.Height) / image.Width));

                    }
                    var filename = Path.GetFileName(image.FileName);
                    image.Save(Server.MapPath("~/UploadedImages/" + filename));
                    filename = Path.Combine("UploadedImages", filename);
                    item.CoverArtUrl = filename;
                }
                if (ModelState.IsValid)
                {


                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {

                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.CategoryId =
               new SelectList(db.Categories, "CategoryId", "Name", item.CategoryId);
            ViewBag.AutorId =
                new SelectList(db.Autors, "AutorId", "Perekonnanimi", item.AutorId);
            return View(item);
        }
        //
        // GET: /Profile/UserInfo
        [ChildActionOnly]
        public ActionResult UserInfo()
        {
            MembershipUser kasutaja = Membership.GetUser();

            return View(kasutaja);
        }
        //
        // GET: /LisaAndmed/5
        public ActionResult LisaAndmed()
        {
            return View();
        }
        //
        // POST: /LisaAndmed/5
        [HttpPost]
        public ActionResult LisaAndmed(KasutajaAndmed kasutajaAndmed)
        {
            MembershipUser kasutaja = Membership.GetUser();
            Guid userID = (Guid)kasutaja.ProviderUserKey;

            if (ModelState.IsValid)
            {
                kasutajaAndmed.UserID = userID;
                db.KasutajaAndmed.Add(kasutajaAndmed);
                db.SaveChanges();



                return RedirectToAction("Index");
            }
            return View();
        }

        //
        // GET: /Andmed/5
        public ActionResult Andmed(Guid userID)
        {
            var tmp = (from x in db.KasutajaAndmed
                       where x.UserID == userID
                       select x.Id).FirstOrDefault();
            var kasutaja = db.KasutajaAndmed.Find(tmp);
            return View(kasutaja);
        }


        // GET: /AddCover
        //public ActionResult addCover(int ItemId)
        //{

        //    return View();

        //}
        //
        // POST: Image
        //public ActionResult addCover(int ItemId, CoverArt cover)
        //{
        //    RaamatuKoguEntities db = new RaamatuKoguEntities();
        //    var raamat = (from x in db.Items
        //                  where x.ItemId == ItemId
        //                  select x).FirstOrDefault();
        //    var image = WebImage.GetImageFromRequest();
        //    if (image != null)
        //    {
        //        if (image.Width > 500)
        //        {
        //            image.Resize(500, ((500 * image.Height) / image.Width));
        //        }

        //        var filename = Path.GetFileName(image.FileName);
        //        image.Save(Path.Combine("../UploadedImages", filename));
        //        filename = Path.Combine("../UploadedImages", filename);

        //        cover.coverArtUrl = Url.Content(filename);

        //        CoverArt pilt = new CoverArt()
        //        {
        //            coverArtUrl = filename,
        //            coverBookTitle = raamat.Title
        //        };

        //        db.CoverArts.Add(pilt);
        //        db.SaveChanges();

        //    }

        //    return View(cover);

        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
