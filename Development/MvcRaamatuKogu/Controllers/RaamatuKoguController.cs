﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcRaamatuKogu.Models;

namespace MvcRaamatuKogu.Controllers
{
    public class RaamatuKoguController : Controller
    {
        
        RaamatuKoguEntities db = new RaamatuKoguEntities();
        //
        // GET: /RaamatuKogu/
        public ActionResult Index()
        {

            var categorys = db.Categories.ToList();
            return View(categorys);

        }

        //
        // GET: /Raamatukogu/Browse
        public ActionResult Browse(int category)
        {

            var categoryModel = db.Items.Where(i => i.CategoryId == category && i.IsPublic == true).ToList();

            return View(categoryModel);



        }
        //
        // GET: /Profile/Details/5
        public ViewResult Book(int id)
        {
            Item item = db.Items.Find(id);
            return View(item);
        }
        //
        // GET: /Raamatukogu/BrowseAutor
        public ActionResult BrowseAutor(string autor)
        {
            using (RaamatuKoguEntities db = new RaamatuKoguEntities())
            {
                var autorItems = (from x in db.Items
                                  where x.Autors.Perekonnanimi == autor
                                  select x).ToList();
                              
                return View(autorItems);
            }
           

            



        }

        //
        // GET: /Store/Categorymenu
        [ChildActionOnly]
        public ActionResult CategoryMenu()
        {
            var categorys = db.Categories.ToList();
            return PartialView(categorys);
        }

        //
        // GET: /Raamatukogu/Details
        public ActionResult Details(int id)
        {
            var item = db.Items.Find(id);

            return View(item);

        }


    }
}
